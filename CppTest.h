#ifndef CppTest_H
#define CppTest_H

class BaseClass
{
public:
        int A;
        int B;
        BaseClass(int a):A(a)
        {}
        int HFileFuntion(int b){return A+b;}
        int CFileFuntion(int b);
        virtual int virtualFunction1(int b)=0;
        virtual int virtualFunction2(int b)=0;
};
class ChildrenClass:public BaseClass
{
public:
  ChildrenClass(int a):BaseClass(a)
  {

  }
  int virtualFunction1(int b)
  {
      return A*b;
  }
  int virtualFunction2(int b);
};
#ifdef __cplusplus
 extern "C" {
#endif

extern   void *ChildrenClassInstanceForC;
extern void CppTest(void *instaceForC);
#ifdef __cplusplus
 }
#endif

#endif
