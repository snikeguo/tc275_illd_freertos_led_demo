#include "CppTest.h"
ChildrenClass ChildrenClassInstance(7);
const ChildrenClass ConstInstance(9);
extern "C"  void *ChildrenClassInstanceForC=(void *)&ChildrenClassInstance;
int BaseClass::CFileFuntion(int b)
{
  return A-b;
}
int ChildrenClass::virtualFunction2(int b)
{
  return A/b;
}
int g_CppValue=10;
extern "C"  void CppTest(void *instaceForC)
{

  BaseClass * p=static_cast<BaseClass *>(instaceForC);
  g_CppValue=p->HFileFuntion(2);
  g_CppValue=p->CFileFuntion(2);
  g_CppValue=p->virtualFunction1(2);
  g_CppValue=p->virtualFunction2(2);
}
